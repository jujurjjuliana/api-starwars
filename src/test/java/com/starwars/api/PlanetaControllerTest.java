package com.starwars.api;

import com.starwars.api.controller.PlanetaController;
import com.starwars.api.model.Planeta;
import com.starwars.api.model.StarWars;
import com.starwars.api.repository.PlanetaRepository;
import com.starwars.api.service.PlanetaService;

import org.springframework.data.mongodb.core.MongoTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;


@WebMvcTest(value=PlanetaController.class)
class PlanetaControllerTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PlanetaService planetaService;
	
	public int quantidadeAparicoes(int id) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "https://swapi.dev/api/planets/" + id + "/";
		StarWars starwars = restTemplate.getForObject(
				url, StarWars.class	
		);		
		return starwars.getFilms().size();
	}
	
	@Test
	public void testCreatePlaneta() throws Exception {
		Planeta mockPlaneta = new Planeta(1, "Tatooine", "arid", "desert", quantidadeAparicoes(1));
		
		String inputInJson = this.mapToJson(mockPlaneta);
		
		String URI = "/api/planetas/create";
		
		Mockito.when(planetaService.createPlaneta(Mockito.any(Planeta.class))).thenReturn(mockPlaneta);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(URI)
				.accept(MediaType.APPLICATION_JSON).content(inputInJson)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		String outputInJson = response.getContentAsString();
				
		assertThat(outputInJson).isEqualTo(inputInJson);
	}
		
	@Test
	public void testGetPlanetaById() throws Exception {
		Planeta mockPlaneta = new Planeta(1, "Tatooine", "arid", "desert", quantidadeAparicoes(1));
				
		Mockito.when(planetaService.getPlanetaById(Mockito.anyInt())).thenReturn(mockPlaneta);
		
		String URI = "/api/planetas/planetaId/1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				URI).accept(
				MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String expectedJson = this.mapToJson(mockPlaneta);
		String outputInJson = result.getResponse().getContentAsString();
				
		assertThat(outputInJson).isEqualTo(expectedJson);
	}	
		
	@Test
	public void testDeletePlaneta() throws Exception {
		Planeta mockPlaneta = new Planeta(1, "Tatooine", "arid", "desert", quantidadeAparicoes(1));
		String result = "Planeta " + mockPlaneta.getId() + " deletado";
				
		String inputInJson = this.mapToJson(result);
		
		Mockito.when(planetaService.deletePlaneta(mockPlaneta.getId())).thenReturn(inputInJson);
		
		String URI = "/api/planetas/planetaId/" + mockPlaneta.getId();
				
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.delete(URI)
				.accept(MediaType.APPLICATION_JSON);
		
		MvcResult r = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = r.getResponse();
		
		String outputInJson = response.getContentAsString();				
		assertThat(outputInJson).isEqualTo(inputInJson);
	}	
	
	@Test
	public void testGetAllPlanetas() throws Exception {
		Planeta mockPlaneta1 = new Planeta(1, "Tatooine", "arid", "desert", quantidadeAparicoes(1));
		Planeta mockPlaneta2 = new Planeta(2, "Alderaan", "temperate", "grasslands, mountains", quantidadeAparicoes(2));
		
		List<Planeta> planetaList = new ArrayList<>();
		planetaList.add(mockPlaneta1);
		planetaList.add(mockPlaneta2);
		
		Mockito.when(planetaService.getAllPlanetas()).thenReturn(planetaList);
		
		String URI = "/api/planetas/allplanetas";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				URI).accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		String expectedJson = this.mapToJson(planetaList);
		String outputInJson = result.getResponse().getContentAsString();
		
		assertThat(outputInJson).isEqualTo(expectedJson);
	}
	
	@Test
	public void testGetPlanetaByName() throws Exception {
		Planeta mockPlaneta1 = new Planeta(1, "Tatooine", "arid", "desert", quantidadeAparicoes(1));
		
		List<Planeta> planetaList = new ArrayList<>();
		planetaList.add(mockPlaneta1);
		
		String expectedJson = this.mapToJson(planetaList);
		
		Mockito.when(planetaService.getPlanetByName(Mockito.anyString())).thenReturn(planetaList);
		
		String URI = "/api/planetas/name/Tatooine";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				URI).accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String outputInJson = result.getResponse().getContentAsString();
		
		assertThat(outputInJson).isEqualTo(expectedJson);
	}
	
	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}
