package com.starwars.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.starwars.api.model.Planeta;
import com.starwars.api.model.StarWars;
import com.starwars.api.repository.PlanetaRepository;


@Configuration
@EnableMongoRepositories(basePackageClasses = {com.starwars.api.repository.PlanetaRepository.class})

@Service
public class PlanetaService{
	
	@Autowired
	private PlanetaRepository planetaRepository;
	
	
	public int quantidadeAparicoes(int id) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "https://swapi.dev/api/planets/" + id + "/";
		StarWars starwars = restTemplate.getForObject(
				url, StarWars.class	
		);
		
		return starwars.getFilms().size();
	}
	
	public Planeta createPlaneta(Planeta planeta) {
		
		Planeta p = new Planeta(planeta.getId(), planeta.getPlanetaNome(), planeta.getPlanetaClima(), planeta.getPlanetaTerreno(), quantidadeAparicoes(planeta.getId()));
		planetaRepository.save(p);
		
		return p;
	}
	
	public Planeta getPlanetaById(Integer planetaId) {
		Optional<Planeta> optionalPlaneta = planetaRepository.findById(planetaId);
		if(optionalPlaneta.isPresent()) {
			return optionalPlaneta.get();
		}
		return null;
	}
	
	public List<Planeta> getAllPlanetas(){
		Iterable<Planeta> result = planetaRepository.findAll();
		List<Planeta> planetasList = new ArrayList<Planeta>();
		result.forEach(planetasList::add);
		
		System.out.println("planetasList " + planetasList);
		return planetasList;
	}
	
	public String deletePlaneta(Integer planetaId) {
		planetaRepository.deleteById(planetaId);
		System.out.println("Planeta service");
		
		return "Planeta " + planetaId +  " deletado";
	}
	
	public Planeta updatePlaneta(Integer planetId, Planeta planeta) {
		Optional<Planeta> optionalPlaneta = planetaRepository.findById(planetId);
		
		Planeta p = optionalPlaneta.get();
		p.setPlanetaNome(planeta.getPlanetaNome());
		p.setPlanetaClima(planeta.getPlanetaClima());
		p.setPlanetaTerreno(planeta.getPlanetaTerreno());
		planetaRepository.save(p);
		
		return p;
	}
	
	public List<Planeta> getPlanetByName(String name) {
		return planetaRepository.findByName(name);
	}
}
