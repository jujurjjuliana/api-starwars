package com.starwars.api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@Document(collection = "Planeta")
public class Planeta {
	
	@Id
	private int id;
	private String planetaNome;
	private String planetaClima;
	private String planetaTerreno;
	private int aparicoesFilmes;	
}
