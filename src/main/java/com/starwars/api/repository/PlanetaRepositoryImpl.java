package com.starwars.api.repository;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.starwars.api.model.Planeta;

@Repository
public class PlanetaRepositoryImpl implements PlanetaRepositoryCustom{
	
	@Inject
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<Planeta> findByName(String planetaNome) {
		Query query = new Query();
		Criteria criteria = Criteria.where("planetaNome").is(planetaNome);
		query.addCriteria(criteria);
		
		List<Planeta> result = mongoTemplate.find(query, Planeta.class, "Planeta");
		
		System.out.println("result = " + result);
		
		return result;
	}

}
