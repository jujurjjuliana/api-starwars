package com.starwars.api.repository;

import java.util.List;

import com.starwars.api.model.Planeta;

public interface PlanetaRepositoryCustom {
	List<Planeta> findByName(String name);
}
