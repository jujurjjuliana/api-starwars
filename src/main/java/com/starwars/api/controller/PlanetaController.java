package com.starwars.api.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.starwars.api.model.Planeta;
import com.starwars.api.service.PlanetaService;


@RestController
@RequestMapping(value="/api/planetas")
public class PlanetaController{
	
	@Autowired
	private PlanetaService planetaService;	
	
	@CrossOrigin
	@PostMapping(value="/create",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public Planeta createPlaneta(@RequestBody Planeta planeta){
		return planetaService.createPlaneta(planeta);
	}
		
	@CrossOrigin
	@GetMapping(value="/planetaId/{planetaId}",produces=MediaType.APPLICATION_JSON_VALUE)
	public Planeta getPlanetaById(@PathVariable("planetaId")Integer planetaId){
		return planetaService.getPlanetaById(planetaId);
	}
	
	@CrossOrigin
	@GetMapping(value="/allplanetas",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Planeta> getAllPlanetas(){
		return planetaService.getAllPlanetas();
	}
	
	@CrossOrigin
	@GetMapping(value="/name/{name:.+}",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Planeta> getPlanetByName(@PathVariable("name")String name){
		return planetaService.getPlanetByName(name);
	}
	
	@CrossOrigin
	@DeleteMapping(value="/planetaId/{planetaId}")
	public String deletePlaneta(@PathVariable("planetaId")Integer planetaId){
		return planetaService.deletePlaneta(planetaId);		
	}
	
	@CrossOrigin
	@PutMapping(value="/planetaId/{planetaId}",produces=MediaType.APPLICATION_JSON_VALUE)
	public Planeta updatePlaneta(@PathVariable("planetaId")Integer planetaId, @RequestBody Planeta planeta){
		return planetaService.updatePlaneta(planetaId, planeta);
	}
}

